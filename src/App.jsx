import React, { useEffect, useState } from "react"
import CurriculumForm from "./components/CurriculumForm"
import EditCurriculum from "./components/EditCurriculum"
import ViewCurriculum from "./components/ViewCurriculum"

const App = () => {
  const [screenIndex, setScreenIndex] = useState(0)
  const [hasCurriculum, setHasCurriculum] = useState(false)

  useEffect(() => {
    setHasCurriculum(JSON.parse(localStorage.getItem('curriculum')))
  }, [])

  const cycleIndex = () => {
    if ((screenIndex + 1) > 2) {
      setScreenIndex(0)
    } else {
      setScreenIndex(screenIndex + 1)
    }
  }

  const cycle = () => {
    return screenIndex === 0 ? 'Editar' : hasCurriculum === false ? 'Criar' :  'Visualizar'
  }

  return (
    (screenIndex === 0) ?
      hasCurriculum ?
        <ViewCurriculum cycleOnClick={cycleIndex} cycleValue={cycle()} /> :
        <CurriculumForm cycleOnClick={cycleIndex} cycleValue={cycle()} hasCurriculum={hasCurriculum} /> :
        <EditCurriculum cycleOnClick={cycleIndex} cycleValue={cycle()} hasCurriculum={hasCurriculum} />
  )
}

export default App
