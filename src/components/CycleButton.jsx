const CycleButton = ({
  onClick,
  value,
}) => {

  return (
    <input
      type='button'
      value={value}
      className="input"
      onClick={onClick}
    />
  )
}

export default CycleButton
