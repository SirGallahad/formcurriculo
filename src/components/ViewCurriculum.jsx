import CycleButton from "./CycleButton.jsx"

const ViewCurriculum = ({
  cycleOnClick,
  cycleValue,
}) => {

  const getCurriculum = () => {
    let curriculum = JSON.parse(localStorage.getItem('curriculum'))

    return{
      name: curriculum.name,
      email: curriculum.email,
      number: curriculum.number,
      role: curriculum.role,
      description: curriculum.description
    }
  }

  return (
    <>
      <h1>Visualizando Currículo</h1>
      <h2>Nome Completo: { getCurriculum().name }</h2>
      <h2>Email: { getCurriculum().email }</h2>
      <h2>Número: { getCurriculum().number }</h2>
      <h2>Cargo: { getCurriculum().role }</h2>
      <h2>Descrição: { getCurriculum().description }</h2>
      <br />
      <CycleButton
        onClick={cycleOnClick}
        value={cycleValue}
      />
    </>
  )
}

export default ViewCurriculum
