import { useForm } from "react-hook-form"
import { ErrorMessage } from '@hookform/error-message';

const CurriculumForm = () => {
  const { register, handleSubmit, formState: { errors } } = useForm()

  const saveForm = (data) => {
    let curriculum = {
      name: data.name,
      email: data.email,
      number: data.number,
      role: data.role,
      description: data.description,
    }

    localStorage.setItem('curriculum', JSON.stringify(curriculum))
    window.location.reload()
  }

  return (
    <>
      <h1>Criando Currículo</h1>
      <form>
        <label htmlFor="name" className='label'>Nome Completo</label>
        <br />
        <input
          className="input"
          placeholder={"João Silva"}
          style={{ width: 300, height: 30 }}
          {...register('name', {
            required: "Insira o nome completo!", pattern: {
              value: /^[a-z]([-']?[a-z]+)*( [a-z]([-']?[a-z]+)*)+$/i,
              message: "Deve ser o nome completo!"
            }
          })}
        />
        <br />
        <div className="error">
          <ErrorMessage errors={errors} name="name" />
        </div>
        <br />
        <br />
        <label htmlFor="email" className='label'>Email</label>
        <br />
        <input
          type={"email"}
          className="input"
          placeholder={"joao@email.com"}
          style={{ width: 300, height: 30 }}
          {...register('email', { required: "Insira o email!" })}
        />
        <br />
        <div className="error">
          <ErrorMessage errors={errors} name="email" />
        </div>
        <br />
        <br />
        <label htmlFor="number" className='label'>Número</label>
        <br />
        <input
          className="input"
          placeholder={"999567218"}
          style={{ width: 300, height: 30 }}
          {...register('number', {
            required: "Insira o número!", pattern: {
              value: /^[0-9]*$/,
              message: "Apenas números!"
            }
          })}
        />
        <br />
        <div className="error">
          <ErrorMessage errors={errors} name="number" />
        </div>
        <br />
        <br />
        <label htmlFor="role" className='label'>Cargo</label>
        <br />
        <input
          className="input"
          placeholder={"Presidente"}
          style={{ width: 300, height: 30 }}
          {...register('role', { required: "Insira o cargo!" })}
        />
        <br />
        <div className="error">
          <ErrorMessage errors={errors} name="role" />
        </div>
        <br />
        <br />
        <label htmlFor="description" className='label'>Descrição</label>
        <br />
        <textarea
          className="input"
          style={{ width: 300, height: 100 }}
          placeholder={"Meu nome é João e sou..."}
          {...register('description', { required: "Insira a descrição!" })}
        />
        <br />
        <div className="error">
          <ErrorMessage errors={errors} name="description" />
        </div>
        <br />
        <br />
        <input
          type="submit"
          value={"Salvar"}
          className="input"
          onClick={handleSubmit(saveForm)}
        />
      </form>
    </>
  )
}

export default CurriculumForm
