import { ErrorMessage } from "@hookform/error-message"
import { useState } from "react"
import { useForm } from "react-hook-form"

const EditCurriculum = () => {
  const [data] = useState(JSON.parse(localStorage.getItem('curriculum')))
  const { register, handleSubmit, formState: { errors } } = useForm({
    defaultValues: {
      name: data.name,
      email: data.email,
      number: data.number,
      role: data.role,
      description: data.description
    }
  })

  const saveForm = (data) => {
    let curriculum = {
      name: data.name,
      email: data.email,
      number: data.number,
      role: data.role,
      description: data.description,
    }

    localStorage.setItem('curriculum', JSON.stringify(curriculum))
    window.location.reload()
  }

  return (
    <>
      <h1>Editando Currículo</h1>
      <form>
        <label htmlFor="name" className='label'>Nome Completo</label>
        <br />
        <input
          className="input"
          placeholder={"João Silva"}
          style={{ width: 300, height: 30 }}
          {...register('name', {
            required: "Insira o nome completo!", pattern: {
              value: /^[a-z]([-']?[a-z]+)*( [a-z]([-']?[a-z]+)*)+$/i,
              message: "Deve ser o nome completo!"
            }
          })}
        />
        <div className="error">
          <ErrorMessage errors={errors} name="name" />
        </div>
        <br />
        <div className='error'>
          <ErrorMessage errors={errors} name="name" />
        </div>
        <br />
        <br />
        <label htmlFor="email" className='label'>Email</label>
        <br />
        <input
          type={"email"}
          className="input"
          placeholder={"joao@email.com"}
          style={{ width: 300, height: 30 }}
          {...register('email', { required: "Insira o email!" })}
        />
        <div className="error">
          <ErrorMessage errors={errors} name="email" />
        </div>
        <br />
        <div className='error'>
          <ErrorMessage errors={errors} name="email" />
        </div>
        <br />
        <br />
        <label htmlFor="number" className='label'>Número</label>
        <br />
        <input
          className="input"
          placeholder={"999567218"}
          style={{ width: 300, height: 30 }}
          {...register('number', {
            required: "Insira o número!", pattern: {
              value: /^[0-9]*$/,
              message: "Apenas números!"
            }
          })}
        />
        <div className="error">
          <ErrorMessage errors={errors} name="number" />
        </div>
        <br />
        <div className='error'>
          <ErrorMessage errors={errors} name="number" />
        </div>
        <br />
        <br />
        <label htmlFor="role" className='label'>Cargo</label>
        <br />
        <input
          className="input"
          placeholder={"Presidente"}
          style={{ width: 300, height: 30 }}
          {...register('role', { required: "Insira o cargo!" })}
        />
        <div className="error">
          <ErrorMessage errors={errors} name="role" />
        </div>
        <br />
        <div className='error'>
          <ErrorMessage errors={errors} name="role" />
        </div>
        <br />
        <br />
        <label htmlFor="description" className='label'>Descrição</label>
        <br />
        <textarea
          className="input"
          style={{ height: 100, width: 300 }}
          placeholder={"Meu nome é João e sou o dono da..."}
          {...register('description', { required: "Insira a descrição!" })}
        />
        <div className="error">
          <ErrorMessage errors={errors} name="description" />
        </div>
        <br />
        <div className='error'>
          <ErrorMessage errors={errors} name="description" />
        </div>
        <br />
        <br />
        <input
          type="submit"
          className="input"
          value={"Salvar alterações"}
          onClick={handleSubmit(saveForm)}
        />
      </form>
    </>
  )
}

export default EditCurriculum
